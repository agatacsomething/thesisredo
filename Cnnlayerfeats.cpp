#include <iostream>
#include "Cnnlayerfeats.h"
#include "opencv2/imgproc/imgproc.hpp"

Cnnlayerfeats::Cnnlayerfeats(){
	std::cout << "Cnnlayerfeats constructor" << std::endl;
}


void Cnnlayerfeats::get_feats(std::vector<cv::Mat> img_mats, CnnArch cnn_arch, int which_layer){
	std::cout << "Cnnlayerfeats get_feats" << std::endl;


	//CONVOLVE ALL IMAGES
	std::vector <cv::Mat> kernels = cnn_arch.kerns[which_layer];
	std::vector< std::vector <cv::Mat> > conv_imgs_all;
	for(std::vector<cv::Mat>::iterator it = img_mats.begin(); it != img_mats.end(); it++){
		cv::Mat img_mat = *it;
		img_mat = img_mat/255;
		std::vector<cv::Mat> conv_imgs;
		conv_imgs = convolve_imgs(img_mat, kernels);
		conv_imgs_all.push_back(conv_imgs);
	}

	//THRESHOLD ALL IMAGES
	std::vector< std::vector<cv::Mat> > thresh_imgs_all;
	for(std::vector< std::vector<cv::Mat> >::iterator it = conv_imgs_all.begin(); it != conv_imgs_all.end(); it++){
		std::vector<cv::Mat> threshed_imgs;
		for(std::vector<cv::Mat>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			cv::Mat temp_img = *it2;
			cv::Mat thresh_img = cv::Mat(temp_img.rows, temp_img.cols, CV_64F);
			thresh_mat(temp_img, thresh_img, 1);
			threshed_imgs.push_back(thresh_img);
		}
		thresh_imgs_all.push_back(threshed_imgs);
	}

	//POOL ALL IMAGES
	std::vector< std::vector<cv::Mat> > pooled_imgs_all;
	for(std::vector< std::vector<cv::Mat> >::iterator it = thresh_imgs_all.begin(); it != thresh_imgs_all.end(); it++){
		std::vector<cv::Mat> pooled_imgs;
		for(std::vector<cv::Mat>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			cv::Mat temp_img = *it2;
			cv::Mat pooled_img = cv::Mat(temp_img.rows/cnn_arch.stride, temp_img.cols/cnn_arch.stride, CV_64F);
			pooling(temp_img, pooled_img, 1, cnn_arch.stride);
			pooled_imgs.push_back(pooled_img);
		}
		pooled_imgs_all.push_back(pooled_imgs);
	}

	//REARRANGE VECT<VECT<MAT>> TO OUTPUT OF VEC<MAT>
	//std::vector<cv::Mat> output_mats;
	for(std::vector< std::vector<cv::Mat> >::iterator it = pooled_imgs_all.begin(); it != pooled_imgs_all.end(); it++){
		for(std::vector<cv::Mat>::iterator it2 = it->begin(); it2 != it->end(); it2++){
			cv::Mat temp_img = *it2;
			output_mats.push_back(temp_img);
		}
	}

	std::cout << "Cnnlayerfeats get_feats end" << std::endl;
}

std::vector<cv::Mat> Cnnlayerfeats::convolve_imgs(cv::Mat img, std::vector<cv::Mat> kernels){

	std::vector<cv::Mat> conv_imgs_temp;
	for(std::vector<cv::Mat>::iterator it = kernels.begin(); it != kernels.end(); it++){
		//cv::Mat conv_img;
		cv::Mat conv_img;
		cv::filter2D(img, conv_img, -1 , *it, cv::Point( -1, -1 ), 0, cv::BORDER_DEFAULT );
		conv_imgs_temp.push_back(conv_img);
	}

	return conv_imgs_temp;
}

double getmax(double a , double b){
	return std::max(a, b);
}

void Cnnlayerfeats::thresh_mat(cv::Mat in_mat, cv::Mat &out_mat, int funct){

	if(funct ==1){
		for(int i=0 ; i < in_mat.rows; i++){
			for(int j=0 ; j < in_mat.cols; j++){
				double temp_val =getmax(0, in_mat.at<double>(i,j));
				out_mat.at<double>(i,j) = temp_val;
			}
		}
	}

}

void Cnnlayerfeats::pooling(cv::Mat in_mat, cv::Mat &out_mat, int pt, int stride){
	//std::cout << "in pooling" <<std::endl;

	if(pt ==1){
		for(int i=0 ; i < in_mat.rows/stride; i++){
			int start_x = i*stride;
			int end_x = start_x+stride;
			for(int j=0 ; j < in_mat.cols/stride; j++){
				int start_y = j*stride;
				int end_y = start_y+stride;
				cv::Mat sub_mat= in_mat.colRange(start_y,end_y).rowRange(start_x,end_x);
				double avg = cv::sum(sub_mat)[0]/(stride*stride);
				out_mat.at<double>(i,j) = avg;
			}
		}
	}
}
