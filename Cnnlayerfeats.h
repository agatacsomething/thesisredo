/**
 * Class: Cnnlayerfeats
 * Description: generates the feature vectors to be fed into the SVM for training and testing
 * Input: All of the images, as a set of vector of matrices, the architecture of the network and the current layer
 * Output: A vector of matrices which have been convolved and pooled
 * Functions:
 * 		get_feats - convolve, threshold and pool all images
 *		thresh_mat - thresholds the matrix after the activation function has been applied
 *		pooling - pools the matrix (so the output matrix goes from 28x28 to 14x14, eg) by applying a pooling function of
 *				  choice. The default pooling function is a mean pooling function
*/


#ifndef CNNLAYERFEATS_H
#define CNNLAYERFEATS_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "cnnsetup.h"


class Cnnlayerfeats{
public:
	Cnnlayerfeats();
	void get_feats(std::vector<cv::Mat> img_mats, CnnArch cnn_arch, int which_layer);

	std::vector<cv::Mat> output_mats;
private:
	std::vector<cv::Mat> convolve_imgs(cv::Mat img, std::vector<cv::Mat> kernels);
	void thresh_mat(cv::Mat in_mat, cv::Mat &out_mat, int funct);
	void pooling(cv::Mat in_mat, cv::Mat &out_mat, int pt, int stride);

};

#endif
