#ifndef GETDATA_H
#define GETDATA_H
#endif

#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;
typedef std::vector<fs::path> Filepaths;
typedef std::vector<std::string> Filenames;

class GenerateData{

public:
	GenerateData();
	GenerateData(fs::path &directory);
	void TestFunction();
	void GetImages();
	void GetDataset(fs::path &directory);

private:
	void GetFileNames (Filenames &fn, fs::path &directory);
	void LoadFiles (Filenames &fn);


};


