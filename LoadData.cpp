#include <iostream>
#include <fstream>
#include "LoadData.h"
#include <boost/algorithm/string.hpp>
#include <math.h>

#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>

#include <opencv2/core/eigen.hpp>

LoadData::LoadData():parsed_data(NULL){
}


void LoadData :: loadDataFile(std::string fn_x, std::string fn_y){
	std::ifstream file_x(fn_x);
	std::ifstream file_y(fn_y);

	if(file_x.is_open() && file_y.is_open()){
		std::cout << "file opened: " << fn_x << std::endl;
		std::cout << "label file opened: " << fn_y << std::endl;
		parsed_data = new ParsedData;
	}

	std::string str_x;
	std::string str_y;

	int i=0;
	//std::ifstream file_xx(fn_x);
	while (std::getline(file_x, str_x))
	{
		if (i<2000){
			parse_line(str_x);
			i++;
		}

		else
			break;
	}

	std::cout << "i= " << i << std::endl;

	i=0;
	//std::ifstream file_yy(fn_y);
	while (std::getline(file_y, str_y))
	{

		if (i<2000){
			parse_line_label(str_y);
			i++;
		}

		else
			break;
	}
	std::cout << "i= " << i << std::endl;
	std::cout << "file parsed" <<std::endl;

}

void LoadData :: parse_line_label(std::string str){
	std::vector<std::string> strs;
	boost::split(strs,str,boost::is_any_of(" "),boost::token_compress_on);
	double val = std::atof(strs[0].c_str());
	parsed_data->y.push_back(val);

}

void LoadData :: parse_line(std::string str){
	std::vector<std::string> strs;
	boost::split(strs,str,boost::is_any_of(" "),boost::token_compress_on);
	int val = numel(strs);
	float siz_im = sqrt(val);
	data2mat(strs);

}

void LoadData::data2mat(std::vector<std::string> strs){
	int val = numel(strs);
	int sz = sqrt(val);
	int i =0;
	cv::Mat_<float> img_mat = cv::Mat_<float>::ones(sz,sz);
	parsed_data->mat_sz = sz;

    for(std::vector<std::string>::iterator it = strs.begin();it!=strs.end();++it){
        int r = i%sz;
        int c = i/sz;
        std::string temp = *it;
        img_mat.at<float>(r,c)=std::stof(temp.c_str());
        i++;

    }
    parsed_data->data_mats.push_back(img_mat.clone());
}

int LoadData ::numel(std::vector<std::string> strs){
	int i =0;
    for(std::vector<std::string>::iterator it = strs.begin();it!=strs.end();++it){
        i++;
    }

    return i;
}

void LoadData ::printMat(const cv::Mat& mat) {
    for(int i = 0; i < mat.rows; i++){
    	for(int j=0; j < mat.cols; j++){
    		std::cout << (int) mat.at<float>(i,j) << " ";
    	}
    	std::cout << std::endl;
    }

}

void LoadData ::printstrs(std::vector<std::string> strs){
    for(std::vector<std::string>::iterator it = strs.begin();it!=strs.end();++it){
        std::cout<<*it<<std::endl;
    }
}
