/**
 * Class: LoadData
 * Description: Loads the data from the given files into structs for the rest of the program
 * Input: file names containing the training data and labels, respectively.
 * Output: A struct with all of the data contained within it.
 * Authors note: the type of storage should be changed to have a vector of structs as opposed to a struct of vectors
 * Functions:
 * 		loadDataFile - loads both the data and its labels from respective files, and checks to see both are present.
 * 						Then goes through the file line by line.
 *		parse_line_label - takes the line from the label file and extracts the label and placed into the struct
 *		parse_line - takes the line from the data file and reshapes it to be stored in a matrix within the struct
 *		data2mat - reshapes the data into matrix
 *		printMat, numel, printstrs - functions for debugging purposes
*/


#ifndef LOADDATA_H
#define LOADDATA_H

#include <string>
#include <vector>
#include <Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

struct ParsedData{
	double mat_sz;
	std::vector<double> y;
	std::vector<cv::Mat> data_mats;
};

class LoadData{
public:
	LoadData();
	void loadDataFile(std::string fn_x, std::string fn_y);

	ParsedData *parsed_data;
private:
	void parse_line(std::string str);
	void parse_line_label(std::string str);
	void printstrs(std::vector<std::string> strs);
	int numel(std::vector<std::string> strs);
	void data2mat(std::vector<std::string> strs);
	void printMat(const cv::Mat& mat);
};


#endif
