#include "cnnsetup.h"
#include <iostream>

Cnnsetup::Cnnsetup(): cnn_arch(NULL){
	std::cout << "Cnnsetup constructor - default" <<std::endl;
}

Cnnsetup::Cnnsetup(int num_layers, int funct, int pooling_type, int stride, std::vector<int> kern_per_layer, int num_ex, int sz_krn){
	cnn_arch = new CnnArch;
	cnn_arch->num_layers = num_layers;
	cnn_arch->funct=funct;
	cnn_arch->pooling_type = pooling_type;
	cnn_arch->stride= stride;
	cnn_arch->kern_per_layer = kern_per_layer;
	cnn_arch->num_train = num_ex;
	cnn_arch->sz_krn=sz_krn;
	genkernels(cnn_arch->kern_per_layer);
}


void Cnnsetup::genkernels(std::vector<int> kernspl){
	std::cout << kernspl.size() <<std::endl;

	for (std::vector<int>::iterator it = kernspl.begin(); it !=kernspl.end(); it++){
		std::vector<cv::Mat> kl;
		std::cout << *it << std::endl;
		for(int i = 0; i< *it; i++){
			cv::Mat_<double> test (cnn_arch->sz_krn,cnn_arch->sz_krn);
			cv::randu(test, 0, 1000);
			test= test/1000;
			kl.push_back(test);
					printMat(kl[i]);
		}
		cnn_arch->kerns.push_back(kl);
	}
}

void Cnnsetup ::printMat(const cv::Mat& mat) {
    for(int i = 0; i < mat.rows; i++){
    	for(int j=0; j < mat.cols; j++){
    		std::cout << (double) mat.at<double>(i,j) << " ";
    	}
    	std::cout << std::endl;
    }

}
