/**
 * Class: Cnnsetup
 * Description: Sets up the architecture for the cnn portion of the training
 * Input: int num_layers - number of layers in the network
 * 		  int funct - the function for the learning - sigmoid, hyperbolic tan, rectified linear unit
 * 		  int pooling_type - type of pooling- eg max, average
 * 		  int stride - the stride for the pooling
 * 		  std::vector< int> kern_per_layer - number of kernels per layer
 * 		  int sz_krn - size of the kernels - eg 3x3, 5x5...
 * Output: A struct with all of the data contained within it, plus the randomly generated kernels
 * Authors note: the type of storage should be changed to have a vector of structs as opposed to a struct of vectors
 * Functions:
 * 		  genkernels - generates the random kernels for training
*/

#ifndef CNNFEATURES_H
#define CNNFEATURES_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

struct CnnArch{
	int num_layers;
	int funct;
	int pooling_type;
	int stride;
	int num_train;
	int sz_krn;
	std::vector<int> kern_per_layer;
	std::vector< std::vector<cv::Mat> > kerns;
};

class Cnnsetup{
public:
	Cnnsetup();
	Cnnsetup(int num_layers, int funct, int pooling_type, int stride, std::vector< int> kern_per_layer, int num_ex, int sz_krn);
	CnnArch *cnn_arch;
	static void printMat(const cv::Mat& mat);
private:
	void genkernels(std::vector < int> kernspl);

};

#endif
