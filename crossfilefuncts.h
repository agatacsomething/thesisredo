#ifndef CROSSFILEFUNCTS_H
#define CROSSFILEFUNCTS_H

#include "linear.h"
#include "svm.h"
#include <vector>
//#include "preprocessdata.h"

typedef std::vector < std::vector<double> > FeatureVec;

class CrossfileFuncts{

public:
	CrossfileFuncts();
	static void MakeSparseFeatures (std::vector<double> &features, svm_node *x_space);
	static void MakeSparseFeatures (std::vector<float> &features, svm_node *x_space);
	static void MakeSparseFeatures (std::vector<double> &features, feature_node *x_space);
	static void MakeSparseFeatures (std::vector<float> &features, feature_node *x_space);
	static void GetSparseFeatLength(std::vector<double> &features, int &num_l);
	static void GetSparseFeatLength(std::vector<float> &features, int &num_l);

	//static void GetSubsampleData(PreprocessData *pd, ParsedData &pd_1, ParsedData &pd_2);
	//static void stdvec2emat(std::vector<std::vector<double> > vec2d, Eigen::MatrixXd &test_mat);
	//static void stdvec2emat(std::vector<double> vec1d, Eigen::MatrixXd &test_mat);
};

#endif
