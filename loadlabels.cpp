#include <iostream>
#include <fstream>
#include "loadlabels.h"
#include <boost/algorithm/string.hpp>
#include <math.h>
#include "cnnsetup.h"


Loadlabels:: Loadlabels(): parsed_labels(NULL){
	std::cout << "constructor for Loadlabels" <<std::endl;
}

void Loadlabels :: loadLabelFile(std::string fn){
	std::ifstream file(fn);


	if(file.is_open()){
		std::cout << "file opened: " << fn << std::endl;
		parsed_labels = new ParsedLabels;
	}

	std::string str;

	std::getline(file, str);
	parse_line(str);
	std::getline(file, str);
	parse_line(str);
	std::cout << "label file parsed" <<std::endl;

}

void Loadlabels :: parse_line(std::string str){
	std::vector<std::string> strs;
	boost::split(strs,str,boost::is_any_of(" "),boost::token_compress_on);
	int val = std::stoi(strs[0].c_str());
	parsed_labels->y.push_back(val);
}
