#ifndef LOADLABELS_H
#define LOADLABELS_H

#include <string>
#include <vector>

struct ParsedLabels{
	double y_sz;
	std::vector<int> y;
};

class Loadlabels{
public:
	Loadlabels();
	void loadLabelFile(std::string fn);

	ParsedLabels *parsed_labels;
private:
	void parse_line(std::string str);
	void printstrs(std::vector<std::string> strs);
	int numel(std::vector<std::string> strs);
//	void data2mat(std::vector<std::string> strs);
	//void printMat(const cv::Mat& mat);
};


#endif
