/**
 * Class: main.cpp
 * Description: The main file for the program.
 * Overall goal: The goal of this program is to train and test data in a hybrid convolutional net/svm training algorithm.
 * 				 This code was not specifically developed to be user friendly, but for my personal purposes.
 * Input: This code is meant to train images for the purpose of classification. The data input should already be split into
 * 		  training and testing data- the training data is further split during training in order to cross validate.
 * 		  The images are assume to be either black and white or else grayscale. Within the input file (train_x or test_x)
 * 		  each line represents the image as a single vector. The labels for each of the data are loaded from separate files
 * 		  (train_y or test_y), with one label per line corresponding to the image train_x.
 * Output: A learned model and the kernels used to generate the model .
 * Classes:
 * 		LoadData - loads the data and stores it in structs
 *		CnnSetup - sets up the architecture for the convnet portion of the model
 *		Cnnlayerfeats - generates the data vectors for each image which are then fed into the svm
 *		Traindata - trains and tests the data
*/


#include <iostream>
#include "boost/filesystem.hpp"
#include <vector>

#include "cnnsetup.h"
#include "LoadData.h"
#include "loadlabels.h"
#include "Cnnlayerfeats.h"
#include "traindata.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


int main(int argc, char *argv[])
{
	std::cout << "helloworld" <<std::endl;
	std::cout << boost::filesystem::current_path().string() << std::endl;

	LoadData *train_data = new LoadData();
	std::string train_file = "train_x_cifar.txt";
	std::string train_label_file = "train_y_cifar.txt";
	train_data->loadDataFile(train_file, train_label_file);

	LoadData *test_data = new LoadData();
	std::string test_file = "test_x_cifar.txt";
	std::string test_label_file = "test_y_cifar.txt";
	test_data->loadDataFile(test_file, test_label_file);

	std::cout << "end of file loading" << std::endl;

	int num_layers = 2;
	int funct =1; //relu
	int pooling_type =1; //average pooling
	int stride = 2;
	int sz_krn =5;
	std::vector<int> kern_per_layer { 3, 6};
	int num_train = train_data->parsed_data->data_mats.size();

	Cnnsetup *cnnset = new Cnnsetup(num_layers, funct, pooling_type, stride, kern_per_layer, num_train, sz_krn);

	Cnnlayerfeats *cnn_feats = new Cnnlayerfeats();
	//if in layer 1
	cnn_feats->get_feats(train_data->parsed_data->data_mats, *cnnset->cnn_arch,0);
	std::vector<cv::Mat> layerfeats = cnn_feats->output_mats;
	//move to next layer
	for(int i = 1; i < cnnset->cnn_arch->num_layers; i++){
		cnn_feats = new Cnnlayerfeats();
		cnn_feats->get_feats(layerfeats, *cnnset->cnn_arch,i);
		layerfeats = cnn_feats->output_mats;
	}

	std::cout << layerfeats.size() << std::endl;

	svm_model *model;
	Traindata *trained_data = new Traindata();
	trained_data->start_training(layerfeats,train_data->parsed_data->y, *cnnset->cnn_arch, model);


	const char *model_file_name = "saved_model.model";
	//int saved= svm_save_model(model_file_name, model);

	model =svm_load_model(model_file_name);

	std::cout << "svm_load_model" << std::endl;

	Cnnlayerfeats *cnn_feats_test = new Cnnlayerfeats();
	//if in layer 1
	cnn_feats_test->get_feats(test_data->parsed_data->data_mats, *cnnset->cnn_arch,0);
	std::vector<cv::Mat> layerfeats_test = cnn_feats_test->output_mats;

	std::cout << "layerfeats_test" << std::endl;
	//move to next layer
	for(int i = 1; i < cnnset->cnn_arch->num_layers; i++){
		cnn_feats_test = new Cnnlayerfeats();
		cnn_feats_test->get_feats(layerfeats_test, *cnnset->cnn_arch,i);
		layerfeats_test = cnn_feats_test->output_mats;
	}

	Traindata *tested_data = new Traindata();
	double train_err, test_err;
	trained_data->start_testing(layerfeats,train_data->parsed_data->y, *cnnset->cnn_arch, model, train_err);
	tested_data->start_testing(layerfeats_test,test_data->parsed_data->y, *cnnset->cnn_arch, model, test_err);
	std::cout << "training error: " << train_err << std::endl;
	std::cout << "testing error: " << test_err << std::endl;




}
