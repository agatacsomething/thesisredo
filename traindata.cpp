#include <iostream>
#include <numeric>
#include "traindata.h"
#include "cnnsetup.h"
#include "crossfilefuncts.h"

Traindata::Traindata(){
	std::cout << "constructor for traindata" << std::endl;
}


void Traindata::start_training(std::vector<cv::Mat> in_data, std::vector<double> labels, CnnArch cnn_arch, svm_model *model){
	std::cout << "starting training" << std::endl;
	std::cout << cnn_arch.num_train << std::endl;

	int subimgs=cnn_arch.num_train;
	for(std::vector<int>::iterator it = cnn_arch.kern_per_layer.begin(); it != cnn_arch.kern_per_layer.end(); it++){
		subimgs = subimgs*(*it);
	}

	int subimgsperex=subimgs/cnn_arch.num_train;

	std::cout << "starting subimgs" << std::endl;
	std::cout << subimgs << std::endl;
	std::cout << subimgsperex << std::endl;
	//int subimgs = cnn_arch.num_train*

	//create feature vector for SVM ----should probably be its own function
	int i =0;
	std::vector< double > vgradx;
	std::vector< std::vector<double > > vgradx_all;
	for(std::vector<cv::Mat>::iterator it = in_data.begin(); it!=in_data.end(); it++){
		cv::Mat temp_img2 = *it;
		cv::Mat temp_img = temp_img2.reshape(0,1);

		if(i==subimgsperex-1){
			i=0;
			for(int j=0; j<temp_img.cols; j++){
				vgradx.push_back(temp_img.at<double>(0,j));
			}
			vgradx_all.push_back(vgradx);
			vgradx.clear();
		}
		else{
			i++;
			for(int j=0; j<temp_img.cols; j++){
				vgradx.push_back(temp_img.at<double>(0,j));
			}
		}
	}

	std::cout << vgradx_all.size() << " : " << vgradx_all[0].size() << std::endl;

	//make feautre sparse
	//initializing the problem
	svm_parameter param;
	defaultparams(param);
	svm_problem prob;
	prob.l = cnn_arch.num_train;
	svm_node** x = new svm_node *[prob.l];

	//double* lab = &v[0];
	std::cout << "bad??????????" << std::endl;
	prob.y= &labels[0];

//	double* lab= new double[prob.l];
////	int v=0;
////	for(std::vector<double>::iterator it = labels.begin(); it!=labels.end(); it++){
////		std::cout << *it <<std::endl;
////		v[0]=*it;
////		std::cout << v[0] <<std::endl;
////	}
//	//std::copy( labels.begin(), labels.end(), prob.y );
//	int num_elements_in_one = sizeof(lab)/sizeof(lab[0]);
//	std::cout << "bad? "<< num_elements_in_one << std::endl;


//	for(std::vector<cv::Mat>::iterator it = in_data.begin(); it!=in_data.end(); it++){
//
//	}

	int m =0;
	for(std::vector< std::vector<double > >::iterator it=vgradx_all.begin() ; it < vgradx_all.end(); it++,i++) {
		//std::cout << "getting data: "<< i  << std::endl;
		std::vector<double> feat_vec = *it;

		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		svm_node *x_space = new svm_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);

		x[m] = x_space;
		m++;
		//delete x_space;
	}
	prob.x = x;

	//svm_model *model;
	std::cout << "being end end end " << std::endl;
	model= svm_train(&prob,&param);
	const char *model_file_name = "saved_model.model";
	int saved= svm_save_model(model_file_name, model);

////	svm_node* testnode;
//	int n =0;
//	std::vector<int> acc_vec;
//	for(std::vector< std::vector<double > >::iterator it=vgradx_all.begin() ; it < vgradx_all.end(); it++,i++) {
//		//std::cout << "getting data: "<< i  << std::endl;
//		std::vector<double> feat_vec = *it;
//
//		int num_l=0;
//		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
//		svm_node *testnode = new svm_node[num_l+1];
//		CrossfileFuncts::MakeSparseFeatures (feat_vec, testnode);
//		double prob_est;
//		//predict_probability(train_model, x_space, prob_est);
//		prob_est= svm_predict(model, testnode);
//		if (remainder(i,100)==0){
//			std::cout << prob_est << " : " << labels[n] << std::endl;
//		}
//
//
//		if (prob_est==labels[n]){
//			acc_vec.push_back(1);
//		}
//		else{
//			acc_vec.push_back(0);
//		}
////		x[m] = testnode;
//		n++;
//		//delete x_space;
//	}
//
//	double sum_of_elems =std::accumulate(acc_vec.begin(),acc_vec.end(),0.0);
//	double acc = sum_of_elems/prob.l;
//	std::cout << "Final accuracy: " << std::endl;
//	std::cout << acc << std::endl;
//	std::cout << "end end end " << std::endl;

}

void Traindata::start_testing(std::vector<cv::Mat> in_data, std::vector<double> labels, CnnArch cnn_arch, svm_model *model, double &err){
	std::cout << "start_testing" << std::endl;

	int subimgs=cnn_arch.num_train;
	for(std::vector<int>::iterator it = cnn_arch.kern_per_layer.begin(); it != cnn_arch.kern_per_layer.end(); it++){
		subimgs = subimgs*(*it);
	}

	int subimgsperex=subimgs/cnn_arch.num_train;

	int i =0;
		std::vector< double > vgradx;
		std::vector< std::vector<double > > vgradx_all;
		for(std::vector<cv::Mat>::iterator it = in_data.begin(); it!=in_data.end(); it++){
			cv::Mat temp_img2 = *it;
			cv::Mat temp_img = temp_img2.reshape(0,1);

			if(i==subimgsperex-1){
				i=0;
				for(int j=0; j<temp_img.cols; j++){
					vgradx.push_back(temp_img.at<double>(0,j));
				}
				vgradx_all.push_back(vgradx);
				vgradx.clear();
			}
			else{
				i++;
				for(int j=0; j<temp_img.cols; j++){
					vgradx.push_back(temp_img.at<double>(0,j));
				}
			}
		}

	//	svm_node* testnode;
		int n =0;
		std::vector<int> acc_vec;
		for(std::vector< std::vector<double > >::iterator it=vgradx_all.begin() ; it < vgradx_all.end(); it++,i++) {
			//std::cout << "getting data: "<< i  << std::endl;
			std::vector<double> feat_vec = *it;

			int num_l=0;
			CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
			svm_node *testnode = new svm_node[num_l+1];
			CrossfileFuncts::MakeSparseFeatures (feat_vec, testnode);
			double prob_est;
			//predict_probability(train_model, x_space, prob_est);
			prob_est= svm_predict(model, testnode);
			if (remainder(i,100)==0){
				std::cout << prob_est << " : " << labels[n] << std::endl;
			}


			if (prob_est==labels[n]){
				acc_vec.push_back(1);
			}
			else{
				acc_vec.push_back(0);
			}
	//		x[m] = testnode;
			n++;
			//delete x_space;
		}

		double sum_of_elems =std::accumulate(acc_vec.begin(),acc_vec.end(),0.0);
		err = sum_of_elems/n;
		std::cout << "Final accuracy: " << std::endl;
		std::cout << err << std::endl;
		std::cout << "end end end " << std::endl;

}

void Traindata::defaultparams(svm_parameter &param){
	//Parameters
	//svm_parameter param;
	param.svm_type = C_SVC;
	param.kernel_type = LINEAR;
	param.degree = 3;
	//param.gamma = 1.0/3780.0;
	param.coef0 = 0;
	//	param.nu = 0.5;
	param.cache_size = 1000;
	param.C = 1;
	param.eps = 1e-3;
	//	param.p = 0.1;
	param.shrinking = 0;
	param.probability = 0;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;
}

