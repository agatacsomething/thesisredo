/**
 * Class: Traindata
 * Description: two functions to train and test the data
 * Input: a vector of images, labels, and the cnn architecture
 * Output: the svm model for training, and the err for testing
 * Functions:
 * 		start_training - transforms the data to libsvm format and trains the data
 *		start_testing - transforms the data to libsvm format and tests the data
 *		defaultparams - sets up the svm training with the default parameters
*/


#ifndef TRAINDATA_H
#define TRAINDATA_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "cnnsetup.h"
#include "svm.h"

class Traindata{
public:
	Traindata();
	void start_training(std::vector<cv::Mat> in_data, std::vector<double> labels, CnnArch cnn_arch, svm_model *model);
	void start_testing(std::vector<cv::Mat> in_data, std::vector<double> labels, CnnArch cnn_arch, svm_model *model, double &err);
private:
	void defaultparams(svm_parameter &param);
};

#endif
